import { Song } from '../../songs/entities/song.entity';
import { Column, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'authors' })
export class Author {
  @PrimaryGeneratedColumn('increment', { name: 'author_id' })
  authorId: number;

  @Column({ name: 'first_name', type: 'varchar' })
  firstName: string;

  @Column({ name: 'last_name', type: 'varchar' })
  lastName: string;

  @Column({ name: 'born', type: 'timestamptz' })
  born: Date;

  @OneToMany(() => Song, (song) => song.author, { nullable: true })
  songs: Song[];
}
