import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Author } from './entities/author.entity';

@Injectable()
export class AuthorsRepository extends Repository<Author> {
  constructor(@InjectRepository(Author) repository: Repository<Author>) {
    super(repository.target, repository.manager, repository.queryRunner);
  }
}
