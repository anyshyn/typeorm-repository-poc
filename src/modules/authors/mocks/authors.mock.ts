import { GetAuthorsListDto } from '../dto/get-authors.dto';

export const mockAuthors: GetAuthorsListDto[] = [
  {
    authorId: 1,
    firstName: 'Theodore',
    lastName: 'Tourot',
    born: new Date('1980-02-08T12:24:39Z'),
  },
  {
    authorId: 2,
    firstName: 'Alexandr',
    lastName: 'Hartil',
    born: new Date('1985-05-19T14:08:11Z'),
  },
  {
    authorId: 3,
    firstName: 'Micheline',
    lastName: 'Pothecary',
    born: new Date('1958-03-30T01:08:31Z'),
  },
  {
    authorId: 4,
    firstName: 'Shayna',
    lastName: 'Seleway',
    born: new Date('1967-08-16T06:41:39Z'),
  },
  {
    authorId: 5,
    firstName: 'Sammie',
    lastName: 'Titchen',
    born: new Date('1958-09-06T03:42:57Z'),
  },
];
