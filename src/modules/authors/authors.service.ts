import { Injectable } from '@nestjs/common';
import { AuthorsRepository } from './authors.repository';
import { CreateAuthorDto } from './dto/create-author.dto';
import { UpdateAuthorDto } from './dto/update-author.dto';

@Injectable()
export class AuthorsService {
  constructor(private readonly authorRepository: AuthorsRepository) {}

  async create(createAuthorDto: CreateAuthorDto) {
    return await this.authorRepository.save(createAuthorDto);
  }

  async findAll(take: number, skip: number) {
    const [items, count] = await this.authorRepository.findAndCount({ take, skip });
    return { items, count };
  }

  async findOne(authorId: number) {
    return await this.authorRepository.findOne({ where: { authorId }, relations: { songs: true } });
  }

  async update(authorId: number, updateAuthorDto: UpdateAuthorDto) {
    return await this.authorRepository.update({ authorId }, updateAuthorDto);
  }

  async remove(authorId: number) {
    return await this.authorRepository.delete({ authorId });
  }
}
