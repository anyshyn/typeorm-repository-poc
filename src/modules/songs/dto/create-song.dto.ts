import { IsNumber, IsString, Length, Min } from 'class-validator';

export class CreateSongDto {
  @Length(1, 128)
  @IsString()
  name: string;

  @Min(1970)
  @IsNumber()
  year: number;

  @Length(1, 64)
  @IsString()
  genre: string;
}
