export class GetSongsListDto {
  songId: number;
  name: string;
  year: number;
  genre: string;
  authorId: number;
}
