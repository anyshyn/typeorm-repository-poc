import { Song } from '../entities/song.entity';
import { SongsRepository } from '../songs.repository';
import { mockSongs } from './songs.mock';

export const mockSongsRepository: jest.Mocked<Partial<SongsRepository>> = {
  findAndCount: jest.fn(async (options) => {
    const paginated = mockSongs.slice(options.skip || 0, options.skip + options.take || mockSongs.length);
    return [paginated, mockSongs.length] as [Song[], number];
  }),
};
