import { DataSource } from 'typeorm';
import dataSourceOptions from './default.ormconfig';

export default new DataSource(dataSourceOptions);
