import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateTableAuthors1664333575539 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    return queryRunner.query(`create table authors (
            author_id SERIAL PRIMARY KEY,
            first_name VARCHAR(50),
            last_name VARCHAR(50),
            born DATE
        );`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    return queryRunner.query(`DROP TABLE authors;`);
  }
}
