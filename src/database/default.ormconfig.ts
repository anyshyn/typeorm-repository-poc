import * as path from 'path';
import { DataSourceOptions } from 'typeorm';

const dataSourceOptions: DataSourceOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'test',
  password: 'test',
  database: 'test',
  migrations: [path.join(__dirname, 'migrations', '*.{ts,js}')],
  entities: [path.join(__dirname, '..', 'modules', '**', '*.entity.{ts,js}')],
  migrationsTableName: 'migrations',
  migrationsRun: true,
};

export default dataSourceOptions;
