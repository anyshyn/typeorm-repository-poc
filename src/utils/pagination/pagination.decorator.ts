import { applyDecorators, Type } from '@nestjs/common';
import { ApiOkResponse, getSchemaPath } from '@nestjs/swagger';

export type ObjectOptions<T extends Type> = {
  model: T;
  description?: string;
};

export type ApiOkPaginatedResponseOptions<T extends Type> = T | ObjectOptions<T>;

export function ApiOkPaginatedResponse<T extends Type>(options: ObjectOptions<T>);
export function ApiOkPaginatedResponse<T extends Type>(model: T);
export function ApiOkPaginatedResponse(options: any) {
  return applyDecorators(
    ApiOkResponse({
      schema: {
        description: options?.description,
        properties: {
          items: { type: 'array', items: { $ref: getSchemaPath(options?.model || options) } },
          count: { type: 'integer', example: 1 },
        },
      },
    }),
  );
}
