import { HttpServer, INestApplication, ValidationPipe } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { SongsModule } from '../src/modules/songs/songs.module';
import * as request from 'supertest';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Song } from '../src/modules/songs/entities/song.entity';
import { mockSongsRepository } from '../src/modules/songs/mocks/songs.repository.mock';
import { SongsRepository } from '../src/modules/songs/songs.repository';
import { mockSongs } from '../src/modules/songs/mocks/songs.mock';
import { TestCase } from 'src/utils/tests/test-case';

const listSongsFailTestCases: TestCase[] = [
  { description: 'no pagination', querystring: '', statusCode: 400 },
  { description: 'only page specified', querystring: '?page=1', statusCode: 400 },
  { description: 'only pageSize specified', querystring: '?pageSize=10', statusCode: 400 },
  { description: 'page is not valid', querystring: '?page=abc&pageSize=10', statusCode: 400 },
  { description: 'pageSize is not valid', querystring: '?page=1&pageSize=abc', statusCode: 400 },
];

const listSongsSuccessTestCases: TestCase[] = [
  {
    description: 'with querystring page=1&perPage=20',
    querystring: '?page=1&pageSize=20',
    statusCode: 200,
    responseBody: { count: 20, items: mockSongs },
  },
  {
    description: 'with querystring page=2&perPage=20',
    querystring: '?page=2&pageSize=20',
    statusCode: 200,
    responseBody: { count: 20, items: [] },
  },
  {
    description: 'with querystring page=1&perPage=10',
    querystring: '?page=1&pageSize=10',
    statusCode: 200,
    responseBody: { count: 20, items: mockSongs.slice(0, 10) },
  },
  {
    description: 'with querystring page=3&perPage=5',
    querystring: '?page=3&pageSize=5',
    statusCode: 200,
    responseBody: { count: 20, items: mockSongs.slice(10, 15) },
  },
];

describe('Songs', () => {
  let app: INestApplication;
  let server: HttpServer;

  beforeAll(async () => {
    const testModule = await Test.createTestingModule({
      imports: [SongsModule],
    })
      .overrideProvider(getRepositoryToken(Song))
      .useValue({})
      .overrideProvider(SongsRepository)
      .useValue(mockSongsRepository)
      .compile();

    app = await testModule
      .createNestApplication()
      .useGlobalPipes(
        new ValidationPipe({
          whitelist: true,
          transform: true,
          forbidNonWhitelisted: true,
          forbidUnknownValues: true,
        }),
      )
      .init();
    server = app.getHttpServer();
  });

  describe('GET /songs', () => {
    describe('Pagination', () => {
      describe.each(listSongsFailTestCases)('Fail flows', (testCase) => {
        test(testCase.description, async () => {
          const response = await request(server).get('/songs' + testCase.querystring);

          expect(response.statusCode).toBe(testCase.statusCode);
        });
      });

      describe.each(listSongsSuccessTestCases)('Success flows', (testCase) => {
        test(testCase.description, async () => {
          const response = await request(server).get('/songs' + testCase.querystring);

          expect(response.statusCode).toBe(testCase.statusCode);
          expect(response.body).toStrictEqual(testCase.responseBody);
        });
      });
    });
  });

  afterAll(() => {
    return app.close();
  });
});
